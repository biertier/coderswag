//
//  CategoryCell.swift
//  CoderSwag
//
//  Created by Tobias Biermeier on 7/30/17.
//  Copyright © 2017 Tobias Biermeier. All rights reserved.
//

import UIKit

class CategoryCell: UITableViewCell {

    @IBOutlet weak var categoryImg: UIImageView!
    @IBOutlet weak var categoryTitle : UILabel!
    
    func updateViews(category: Category) {
        categoryImg.image = UIImage(named: category.imageName)
        categoryTitle.text = category.title
    }

}
