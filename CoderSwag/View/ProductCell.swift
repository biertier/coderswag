//
//  ProductCell.swift
//  CoderSwag
//
//  Created by Tobias Biermeier on 7/30/17.
//  Copyright © 2017 Tobias Biermeier. All rights reserved.
//

import UIKit

class ProductCell: UICollectionViewCell {
    
    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var productTitel: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    
    func updateViews(product: Product) {
        productImg.image = UIImage(named: product.imageName)
        productTitel.text = product.title
        productPrice.text = product.price
    }
    
}
