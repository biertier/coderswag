//
//  Category.swift
//  CoderSwag
//
//  Created by Tobias Biermeier on 7/30/17.
//  Copyright © 2017 Tobias Biermeier. All rights reserved.
//

import Foundation

struct Category {
    
    // Make variables private to set in this struct, but public to retrieve
    private(set) public var title: String
    private(set) public var imageName: String
    
    init(title: String, imageName: String) {
        self.title = title
        self.imageName = imageName
    }
    
}


















